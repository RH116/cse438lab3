//
//  PropertyPath.swift
//  RenhaoDrawing
//
//  Created by Renhao on 5/16/18.
//  Copyright © 2018 Renhao. All rights reserved.
//

import Foundation
import UIKit
class PropertyPath:UIBezierPath{
    let color:UIColor
    let opacity:CGFloat
    let startPoint:CGPoint
    var endPoint:CGPoint?
    var isDot = true
    init(color:UIColor,opacity:CGFloat,startPoint:CGPoint) {
        self.color = color
        self.opacity = opacity
        self.startPoint = startPoint
        super.init()
    }
    func updateEndPoint(endPoint:CGPoint){
        self.endPoint = endPoint
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func changeColor(){
        color.setStroke()
        color.setFill()
    }
    override func stroke() {
        changeColor()
        super.stroke(with: CGBlendMode.normal, alpha: opacity)
    }
}
