//
//  Stack.swift
//  RenhaoDrawing
//
//  Created by Renhao on 5/16/18.
//  Copyright © 2018 Renhao. All rights reserved.
//

import Foundation
// code from https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/Generics.html

protocol Container {
    associatedtype Item
    mutating func append(_ item: Item)
    var count: Int { get }
    subscript(i: Int) -> Item { get }
}
struct Stack<Element>: Container {
    
    // original Stack<Element> implementation
    var items = [Element]()
    mutating func push(_ item: Element) {
        items.append(item)
    }
    mutating func pop() -> Element? {
        guard !items.isEmpty else{return nil}
        return items.removeLast()
    }
    mutating func removeAll(){
        items.removeAll()
    }
    // conformance to the Container protocol
    mutating func append(_ item: Element) {
        self.push(item)
    }
    var count: Int {
        return items.count
    }
    subscript(i: Int) -> Element {
        return items[i]
    }
}
