//
//  DrawView.swift
//  RenhaoDrawing
//
//  Created by Renhao on 5/16/18.
//  Copyright © 2018 Renhao. All rights reserved.
//

import UIKit

class DrawView: UIView {
    
    var lines:Stack<PropertyPath>?
    var currentPath:PropertyPath?
    func update(currentLine:PropertyPath?,historicLine:Stack<PropertyPath>){
        self.currentPath = currentLine
        self.lines = historicLine
        setNeedsDisplay()
    }
    override func draw(_ rect: CGRect) {
        if let linesSafe = lines{
            for lineIndex in 0..<linesSafe.count{
                let line = linesSafe.items[lineIndex]
                line.changeColor()
                if(!line.isDot){
                    line.stroke()
                }else{
                    drawDots(line: line)}
            }
        }
        if let currentPathSafe = currentPath {
            currentPathSafe.changeColor()
            if(!currentPathSafe.isDot){
                currentPathSafe.stroke()
            }
            else{
                drawDots(line: currentPathSafe)}
            
        }
    }
    func drawDots(line:PropertyPath){
        drawDot(lastLocation: line.startPoint, lineWidth: line.lineWidth, alpha: line.opacity)
        //drawDot(lastLocation: line.endPoint!, lineWidth: line.lineWidth, alpha: line.opacity)
    }
    func drawDot(lastLocation:CGPoint, lineWidth:CGFloat,alpha:CGFloat){
        let path = UIBezierPath()
        path.addArc(withCenter: lastLocation, radius: lineWidth/2, startAngle: 0, endAngle: CGFloat.pi*2, clockwise: true)
        path.fill(with: CGBlendMode.normal, alpha:alpha )
    }
    
    
    
}
