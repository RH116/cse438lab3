//
//  ColorBtn.swift
//  RenhaoDrawing
//
//  Created by Renhao on 5/16/18.
//  Copyright © 2018 Renhao. All rights reserved.
//

import UIKit

@IBDesignable class ColorBtn: UIButton {
    
    let borderColor:UIColor = UIColor.black
    let transparent = UIColor(white: 1, alpha: 0)
    var isActive:Bool?{
        didSet{
            setNeedsDisplay()
        }
    }
    func active(){
        isActive = true
    }
    func deactive(){
        isActive = false
    }
    @IBInspectable var fillColor:UIColor = UIColor.purple
    override func awakeFromNib() {
        
        frame.size = CGSize(width: 50, height: 50)
        self.titleLabel?.text = ""
        self.backgroundColor = transparent
        //        layer.cornerRadius = 0.5 * self.bounds.size.width
        //        self.layer.borderWidth = self.bounds.size.width/15
        //        layer.borderColor = borderColor.cgColor
    }
    
    override func draw(_ rect: CGRect) {
        let fillPath = UIBezierPath(arcCenter: CGPoint(x: 25,y: 25), radius: CGFloat(15), startAngle: CGFloat(0), endAngle:CGFloat(Double.pi * 2), clockwise: true)
        self.fillColor.setFill()
        fillPath.fill()
        if isActive ?? false {
            let borderPath = UIBezierPath(arcCenter: CGPoint(x: 25,y: 25), radius: CGFloat(17), startAngle: CGFloat(0), endAngle:CGFloat(Double.pi * 2), clockwise: true)
            self.borderColor.setStroke()
            borderPath.stroke()
        }

        
    }
    
    
}
