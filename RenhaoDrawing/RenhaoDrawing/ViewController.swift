//
//  ViewController.swift
//  RenhaoDrawing
//
//  Created by Renhao on 5/16/18.
//  Copyright © 2018 Renhao. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var drawBoard: DrawView!
    
    
    @IBAction func opcitySlider(_ sender: UISlider) {
        currentOpacity = CGFloat(sender.value)
    }
    @IBOutlet weak var opcitySlider: UISlider!
    @IBOutlet weak var widthSlider: UISlider!
    @IBAction func widthSlider(_ sender: UISlider) {
        currentWidth = CGFloat(sender.value)
    }
    @IBOutlet var colorBtns: [ColorBtn]!
    @IBAction func ColorBtnClick(_ sender: ColorBtn) {
        for btn in colorBtns{
            btn.deactive()
        }
        sender.active()
        currentColor = sender.fillColor
    }
    
    var lastPoint:CGPoint!
    var currentPath:PropertyPath?{
        didSet{
            updateUI()
        }
    }
    var lines:Stack = Stack<PropertyPath>(){
        didSet{
            currentPath = nil
            updateUI()
        }
    }
    var redoLines:Stack = Stack<PropertyPath>()
    var currentPoints:[CGPoint]?
    var currentColor:UIColor?
    var currentWidth:CGFloat?
    var currentOpacity:CGFloat?
    var isDot = true
    @IBAction func clear(_ sender: UIButton) {
        lines.removeAll()
        redoLines.removeAll()
    }
    @IBAction func undo(_ sender: UIButton) {
        if let redoline = lines.pop(){
            redoLines.push(redoline)
        }
    }
    @IBAction func redo(_ sender: UIButton) {
        if let redoline = redoLines.pop(){
            lines.push(redoline)
        }
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        currentPoints = [CGPoint]()
        lastPoint = touches.first?.location(in: drawBoard)
        currentPoints?.append(lastPoint)
        isDot = true
    }
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        lastPoint = touches.first?.location(in: drawBoard)
        currentPoints?.append(lastPoint)
        if distanceBetween(a: currentPoints!.first!, b: currentPoints!.last!)>currentWidth! {
            isDot = false
        }
        currentPath = createQuadPath(points: currentPoints!)
    }
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        lastPoint = touches.first?.location(in: drawBoard)
        currentPoints?.append(lastPoint)
        currentPath = createQuadPath(points: currentPoints!)
        lines.push(currentPath!)
    }
    
    func updateUI(){
        drawBoard.update(currentLine: currentPath, historicLine: lines)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        currentOpacity = 1.0
        currentWidth = 5.0
        currentColor = UIColor.red
        opcitySlider.value = Float(currentOpacity!)
        widthSlider.maximumValue = 20
        widthSlider.value = Float(currentWidth!)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    private func midpoint(first: CGPoint, second: CGPoint) -> CGPoint {
        let midX = (first.x+second.x)/2
        let midY = (first.y+second.y)/2
        let newPoint = CGPoint(x:midX,y:midY)
        return newPoint
    }
    
    func createQuadPath(points: [CGPoint]) -> PropertyPath {
        print(points.count)
        let safeColor = currentColor ?? UIColor.black
        let safeOpacity = currentOpacity ?? 1.0
        let safeWidth = currentWidth ?? 1
        let path = PropertyPath(color: safeColor, opacity: safeOpacity,startPoint:points[0])
        path.updateEndPoint(endPoint: currentPoints![currentPoints!.count-1])
        path.lineWidth = safeWidth
        let firstPoint = points[0]
        path.move(to: firstPoint)
        if points.count <= 2 { return path }
        
        let secondPoint = points[1]
        let firstMidpoint = midpoint(first: firstPoint, second: secondPoint)
        path.addLine(to: firstMidpoint)
        for index in 1 ..< points.count-1 {
            let currentPoint = points[index]
            let nextPoint = points[index + 1]
            let midPoint = midpoint(first: currentPoint, second: nextPoint)
            path.addQuadCurve(to: midPoint, controlPoint: currentPoint)
        }
        guard let lastLocation = points.last else { return path }
        path.addLine(to: lastLocation)
        path.isDot = isDot
        return path
    }
    
    func distanceBetween(a: CGPoint, b: CGPoint) -> CGFloat {
        let xDist = a.x - b.x
        let yDist = a.y - b.y
        return CGFloat(sqrt((xDist * xDist) + (yDist * yDist)))
    }
    
    
}

